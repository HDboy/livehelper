const express = require('express');
const router = express.Router();
const admin = require('../admin');


router.post('/', async (req, res) => {

    try {
        const { uid, opid, npid } = req.body;
        // Download file from bucket.
        const bucket = admin.storage().bucket();
        const result = await bucket.file(`prouduct/${uid}/${opid}`).copy(`prouduct/${uid}/${npid}`);
        const data = result[1].resource;
        const token = data.metadata.firebaseStorageDownloadTokens;
        const returnUrl = `https://firebasestorage.googleapis.com/v0/b/${data.bucket}/o/${encodeURIComponent(data.name)}?alt=media&token=${token.toString().split(',')[0]}`;
        console.log(returnUrl);
        res.status(200).send({ returnUrl });

    } catch (e) {
        console.error(e.massage);
        res.status(500).send(e.message);
    }

});


module.exports = router;