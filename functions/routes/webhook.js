const express = require('express');
const router = express.Router();
const request = require('request');
const functions = require('firebase-functions');
const access_token = functions.config().access_token;
const PAGE_ACCESS_TOKEN = {

    1675636639364438: access_token.page_id_1675636639364438
    // 1953334474915225: access_token.page_id_1953334474915225,
    // 174833995883377: access_token.page_id_174833995883377
}

// Accepts POST requests at /webhook endpoint
router.post('/', (req, res) => {

    // Parse the request body from the POST
    const body = req.body;

    // Check the webhook event is from a Page subscription
    if (body.object === 'page') {

        body.entry.forEach((entry) => {

            // Gets the body of the webhook event
            let webhook_event = entry.messaging[0];

            const pageId = entry.id;
            console.log('page id:', pageId);


            // Get the sender PSID
            console.log(webhook_event.sender)
            let sender_psid = webhook_event.sender.id;
            console.log('Sender ID: ' + sender_psid);

            // Check if the event is a message or postback and
            // pass the event to the appropriate handler function
            if (webhook_event.message) {
                handleMessage(sender_psid, webhook_event.message, pageId);
            } else if (webhook_event.postback) {

                handlePostback(sender_psid, webhook_event.postback, pageId);
            }

        });
        // Return a '200 OK' response to all events
        res.status(200).send('EVENT_RECEIVED');

    } else {
        // Return a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }

});

// Accepts GET requests at the /webhook endpoint
router.get('/', (req, res) => {

    /** UPDATE YOUR VERIFY TOKEN **/
    const VERIFY_TOKEN = "hady_live_helper";

    // Parse params from the webhook verification request
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    // Check if a token and mode were sent
    if (mode && token) {

        // Check the mode and token sent are correct
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Respond with 200 OK and challenge token from the request
            console.log('WEBHOOK_VERIFIED');
            res.status(200).send(challenge);

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            res.sendStatus(403);
        }
    }
});

// Handles messages events
function handleMessage(sender_psid, received_message, pageId) {
    let response;

    // Checks if the message contains text
    // if (received_message.text) {
    //     // Create the payload for a basic text message, which
    //     // will be added to the body of our request to the Send API
    //     response = {
    //         "text": `You sent the message: "${received_message.text}". Now send me an attachment!`
    //     }
    // } else 
    if (received_message.attachments) {
        // Get the URL of the message attachment
        let attachment_url = received_message.attachments[0].payload.url;
        response = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [{
                        "title": "ใช่ใบเสร็จของคุณหรือไม่ ?",
                        "subtitle": "แตะปุ่มเพื่อตอบ",
                        "image_url": attachment_url,
                        "buttons": [
                            {
                                "type": "postback",
                                "title": "ใช่",
                                "payload": attachment_url
                            },
                            {
                                "type": "postback",
                                "title": "ไม่ใช่",
                                "payload": "no"
                            }
                        ],
                    }]
                }
            }
        }
    }

    // Send the response message
    callSendAPI(sender_psid, response, pageId);
}

// Handles messaging_postbacks events
function handlePostback(sender_psid, received_postback, pageId) {
    console.log('ok')
    // console.log(received_postback);
    let response;
    // Get the payload for the postback
    let payload = received_postback.payload;

    // Set the response based on the postback payload
    if (payload !== 'no') {
        response = { "text": "ขอบคุณค่ะ" }
    } else {
        response = { "text": "ถ้าโอนเงินเรียบร้อยแล้วกรุณาแนบใบเสร็จด้วยค่ะ" }
    }
    // Send the message to acknowledge the postback
    callSendAPI(sender_psid, response, pageId);
}

// Sends response messages via the Send API
function callSendAPI(sender_psid, response, pageId) {
    // Construct the message body
    let request_body = {
        "recipient": {
            "id": sender_psid
        },
        "message": response
    }

    // Send the HTTP request to the Messenger Platform
    request({
        "uri": "https://graph.facebook.com/v7.0/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN[pageId] },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
            console.log('message sent!')
        } else {
            console.error("Unable to send message:" + err);
        }
    });
}

module.exports = router;