const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');
const webhook = require('./routes/webhook');
const firebaseStorage = require('./routes/firebaseStorage');
const cors = require('cors');
const auth = require('./middleware/auth');

const app = express();


exports.app = functions.region('asia-east2').https.onRequest(app);
app.use(cors());
app.use(bodyParser.json());
app.use('/webhook', webhook);
app.use('/firebaseStorage',auth, firebaseStorage);


