const admin = require('../admin');

module.exports = async (req, res, next) => {
    if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
        console.log('Unauthorized');
        res.status(403).send('Unauthorized');
        return;
    }
    const idToken = req.headers.authorization.split('Bearer ')[1];
    try {
        // console.log(idToken);
        const decodedIdToken = await admin.auth().verifyIdToken(idToken);
        // console.log(decodedIdToken);
        req.user = decodedIdToken;
        next();
        return;
    } catch (e) {
        console.error(e.message);
        res.status(403).send('Unauthorized');
        return;
    }
};
