// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC9fhXGPWdMv9N3iXmeQREO_N7gK0-i0Hk',
    authDomain: 'livehelper-hady.firebaseapp.com',
    databaseURL: 'https://livehelper-hady.firebaseio.com',
    projectId: 'livehelper-hady',
    storageBucket: 'livehelper-hady.appspot.com',
    messagingSenderId: '1088185434455',
    appId: '1:1088185434455:web:e71f50c0b2b8d288bf3e12',
    measurementId: 'G-0MQEH1FLDZ'
  },
  app_id: 644267946158835,
  fb_api_url: 'https://graph.facebook.com',
  fb_api_stream_url: 'https://streaming-graph.facebook.com',
  doc_app_secret: '7NKqfCL0mDb7q70TwyTf',
  cloud_funtions_api_url: 'http://localhost:5000/livehelper-hady/asia-east2/app'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
