import { PolicyComponent } from './components/policy/policy.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ManageProductsComponent } from './components/manage-products/manage-products.component';
import { SettingComponent } from './components/setting/setting.component';
import { OrderComponent } from './components/order/order.component';
import { AuthGuard } from './services/auth-guard.service';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'policy',
    component: PolicyComponent
  },
  {
    path: 'products',
    component: ManageProductsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'product/new',
    component: ProductFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'product/edit/:id',
    component: ProductFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'setting',
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'order',
    component: OrderComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
