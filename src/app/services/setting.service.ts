import { take } from 'rxjs/operators';
import { Observable, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { SettingModel } from '../models/setting-model';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(
    private afs: AngularFirestore
  ) { }

  create(uid: string, setting: SettingModel) {
    return from(this.afs.doc<SettingModel>(`settings/${uid}`).set(setting))
      .pipe(take(1));
  }

  get(uid: string): Observable<SettingModel> {
    return this.afs.doc<SettingModel>(`settings/${uid}`).valueChanges();
  }

  // update(uid: string, setting: SettingModel) {
  //   return this.afs.doc<SettingModel>(`items/${uid}`).update(setting);
  // }
}
