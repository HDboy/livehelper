import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { IconSnackBarComponent } from '../components/icon-snack-bar/icon-snack-bar.component';
import { faTimesCircle, faCheckCircle } from '@fortawesome/free-solid-svg-icons';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) { }

  error(value: string) {
    this.snackBar.openFromComponent(IconSnackBarComponent, {
      data: {
        message: value,
        icon: faTimesCircle,
        action: this.translate.instant('common.close')
      },
      duration: 2500,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: 'error-snackbar'
    });
  }

  success(value: string) {
    this.snackBar.openFromComponent(IconSnackBarComponent, {
      data: {
        message: value,
        icon: faCheckCircle,
        action:  this.translate.instant('common.close')
      },
      duration: 2500,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: 'success-snackbar'
    });
  }

  info(value: string) {
    this.snackBar.open(value, 'Close', {
      duration: 2500,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: 'info-snackbar'
    });
  }

  warning(value: string) {
    this.snackBar.open(value, 'Close', {
      duration: 2500,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: 'warning-snackbar '
    });
  }


}

