import { OrderService } from './order.service';
import { SettingModel } from './../models/setting-model';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FbService } from 'src/app/services/fb.service';

import { FbPageModel } from './../models/fb-page-model';
import { ProductModel } from './../models/product-model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  private productsCollection: AngularFirestoreCollection<ProductModel>;
  // products$: Observable<ProductModel[]>;

  constructor(
    private afs: AngularFirestore,
    private fbService: FbService,
    private orderService: OrderService
  ) {
    this.productsCollection = afs.collection<ProductModel>('products');
    // this.products$ = this.productsCollection.valueChanges({ idField: 'id' });
  }

  add(product: ProductModel, pid: string) {
    return from(
      this.productsCollection.doc(pid).set(product)
    );
  }

  getProductByUserId(uid: string) {
    return this.afs.collection<ProductModel>('products', ref => ref.where('uid', '==', uid))
      .valueChanges({ idField: 'id' });
  }

  update(product: ProductModel, pid: string) {
    return from(
      this.productsCollection.doc(pid).update(product)
    );
  }

  deleteFromId(id) {
    return from(
      this.productsCollection.doc(id).delete()
    );
  }

  createId() {
    return this.afs.createId();
  }

  getById(Id): Observable<ProductModel> {
    return this.productsCollection.doc<ProductModel>(Id).valueChanges();
  }

  checkStock(products, code: string, fbid, page: FbPageModel, userName: string, setting: SettingModel) {
    const product = products.find(
      p => code.toLowerCase().includes(p.code.toLowerCase()) &&
        code.toLowerCase().includes(p.color ? p.color.toLowerCase() : '') &&
        code.toLowerCase().includes(p.size ? p.size.toLowerCase() : '')
    );

    if (product) {
      // console.log('product', product);
      if ((product.quantity as number) <= 0) {
        console.log(`${product.title} ${product.code} out of stock`);
        return;
      }
      console.log(`${product.title} ${product.code} reduce stock`);
      this.productsCollection.doc(product.id).update({
        quantity: (product.quantity - 1)
      }).then(() => {
        let psid;
        this.fbService.getPSID(fbid, page.id)
          .pipe(switchMap(result => {
            psid = (result as any).data[0].id;
            console.log(psid);
            return this.fbService.sendMessage(page.access_token, psid, product, page, userName, setting);
          })).subscribe(result => {
            console.log(result);
            this.orderService.add({
              product,
              user: {
                id: fbid,
                name: userName,
                psid
              },
              uid: product.uid,
              date: new Date(),
              id: this.afs.createId(),
              page: {
                id: page.id,
                name: page.name
              }
            });
          });
      });
    }
  }

}
