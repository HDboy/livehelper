import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SsrService {

  getEventSource(url: string): EventSource {
    return new EventSource(url);
  }
}
