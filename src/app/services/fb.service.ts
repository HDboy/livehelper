import { SettingModel } from './../models/setting-model';
import { ProductModel } from './../models/product-model';
import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, from, of, BehaviorSubject } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { FbPageModel } from '../models/fb-page-model';
import { SsrService } from './ssr.service';

@Injectable({
  providedIn: 'root'
})
export class FbService {

  private dataSource = new BehaviorSubject<boolean>(false);
  isConnected$ = this.dataSource.asObservable();
  eventSource: EventSource;

  constructor(
    private zone: NgZone,
    private ssr: SsrService,
    private http: HttpClient,
    private afs: AngularFirestore
  ) { }


  getLiveComment(liveId: string, userAccessToken: string): Observable<{}> {
    // tslint:disable-next-line:max-line-length
    // userAccessToken = 'EAAHw2tnm4ZAYBAJ7Pbw5ZCaylajQLPGQgnpEdnEgVr7O6nCSI7hSFiHKYbBEAFdSpZA6dCUzi4CFibwB3WcZC7MwmZCIUz7tNgp0GCUtsore0eVG9fJmPOyaw2XDNlwZCyx7i7vSMhCt1A1309YDE7H63ZAcmcUmIOZBaqy2tbNnu9lriqRYsv58kKEEPoBkUzwPBZAHzp2umBAZDZD';
    const liveCommentApiUrl = `${environment.fb_api_stream_url}/${liveId}/live_comments?access_token=${userAccessToken}&comment_rate=one_per_two_seconds&fields=from{name,id},message`;
    return Observable.create(observer => {

      this.eventSource = this.ssr.getEventSource(liveCommentApiUrl);

      this.eventSource.onmessage = event => {
        this.zone.run(() => {
          observer.next(event);
        });
      };

      this.eventSource.onerror = error => {
        this.zone.run(() => {
          observer.error(error);
        });
      };

      this.eventSource.onopen = () => {
        this.zone.run(() => {
          this.dataSource.next(true);
        });
        console.log('Connection to server opened.');
      };

    });
  }

  stopLiveComment() {
    this.eventSource.close();
    console.log('Close connection.');
    this.dataSource.next(false);
  }

  getPage(fbid: string, fbAccessToken: string): Observable<FbPageModel[]> {
    return this.http.get(`${environment.fb_api_url}/${fbid}/accounts?fields=name,access_token&access_token=${fbAccessToken}`)
      .pipe(take(1))
      .pipe(map(result => (result as any).data));
  }

  getPSID(fbid: string, pageId: string) {
    const appAccessToken = localStorage.getItem('appAccessToken');
    return this.http.get(`${environment.fb_api_url}/${fbid}/ids_for_pages?page=${pageId}&access_token=${appAccessToken}`)
      .pipe(take(1));
  }

  getAppToken() {
    return this.afs.collection('app').doc(environment.doc_app_secret).valueChanges()
      .pipe(take(1))
      .pipe(switchMap(result => {
        const appSecret = (result as any).appSecret;
        console.log(appSecret);
        return this.http.get(`${environment.fb_api_url}/oauth/access_token?client_id=${environment.app_id}&client_secret=${appSecret}&grant_type=client_credentials`)
          ;
      }))
      .pipe(take(1));
  }

  sendMessage(
    pageAccessToken: string,
    PSID: string,
    product: ProductModel,
    page: FbPageModel,
    userName: string,
    setting: SettingModel
  ) {
    const color = product.color ? ` สี${product.color}` : '';
    const size = product.size ? ` ขนาด${product.size}` : '';
    return this.http.post(`${environment.fb_api_url}/v7.0/me/messages?access_token=${pageAccessToken}`,
      {
        messaging_type: 'MESSAGE_TAG',
        tag: 'POST_PURCHASE_UPDATE',
        recipient: {
          id: PSID
        },
        message: {
          text: `เรียนคุณ ${userName}\nได้ทำการสั่งซื้อสินค้า ${product.title}\nรหัสสินค้า ${product.code}${color}${size} ราคา ${product.price}บาท\nจาก ${page.name} \n\n${setting.bankAccount} \n\n${setting.thanksMessage} `
        }
      }).pipe(take(1));
  }

}
