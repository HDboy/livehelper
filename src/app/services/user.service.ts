import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { UserModel } from './../models/user-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersCollection: AngularFirestoreCollection<UserModel>;
  users$: Observable<UserModel[]>;

  constructor(private afs: AngularFirestore) {
    this.usersCollection = afs.collection<UserModel>('users');
    this.users$ = this.usersCollection.valueChanges();
  }


  async create(user: UserModel) {
    try {
      await this.usersCollection.doc(user.uid).set(user);
      return true;
    }
    catch (error) {
      console.error(error.message);
      return false;
    }
  }

}
