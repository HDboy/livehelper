import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { from, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { FbService } from 'src/app/services/fb.service';

import { ProgressBarService } from './progress-bar.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<firebase.User>;

  constructor(
    private afAuth: AngularFireAuth,
    private userService: UserService,
    private fbService: FbService,
    private router: Router,
    private progressBar: ProgressBarService
  ) {
    this.user$ = afAuth.authState;
  }

  loginWithFb() {
    this.afAuth.signInWithRedirect(new firebase.auth.FacebookAuthProvider());
  }

  logout() {
    return from(this.afAuth.signOut()).pipe(take(1));
  }

  currentIdToken(): Observable<string> {
    return this.afAuth.idToken
      .pipe(take(1));
  }

  async getRedirect() {
    try {
      this.progressBar.showProgressBar(true);
      const result: firebase.auth.UserCredential = await this.afAuth.getRedirectResult();
      console.log(result);
      if (result.additionalUserInfo?.isNewUser) {
        const { uid, email, phoneNumber } = result.user;
        const fbid = (result.additionalUserInfo?.profile as any).id;
        this.userService.create({
          uid,
          email,
          phoneNumber,
          role: 'user',
          fbid
        });
      }
      if (result.credential) {
        localStorage.setItem('fbAccessToken', (result.credential as any).accessToken);
        this.fbService.getAppToken().subscribe(data => {
          console.log(data);
          localStorage.setItem('appAccessToken', (data as any).access_token);
        });

        const returnUrl = localStorage.getItem('returnUrl');

        if (returnUrl) {
          localStorage.removeItem('returnUrl');
          this.router.navigateByUrl(returnUrl);
        }
      }

      this.progressBar.showProgressBar(false);
    }

    catch (err) {
      console.error(err.message);
    }
  }

}
