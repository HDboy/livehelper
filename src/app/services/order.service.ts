import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { OrderModel } from './../models/order-model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private ordersCollection: AngularFirestoreCollection<OrderModel>;
  constructor(private afs: AngularFirestore) {
    this.ordersCollection = afs.collection<OrderModel>('orders');
  }

  add(order: OrderModel) {
    return this.ordersCollection.doc(order.id).set(order);
  }

  get(uid: string): Observable<OrderModel[]> {
    const refFn = ref => ref.where('uid', '==', uid);
    return this.afs.collection<OrderModel>('orders', refFn)
      .valueChanges();

  }
}
