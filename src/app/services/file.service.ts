import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference } from '@angular/fire/storage';
import * as loadImage from 'blueimp-load-image';
import { from, Observable, of } from 'rxjs';
import { last, switchMap, map, take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FileService {

  loadImgOptions = {
    orientation: true,
    maxHeight: 800,
    canvas: true
  };

  private ref: AngularFireStorageReference;

  constructor(
    private storage: AngularFireStorage,
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  uploadProductImg(file, uid: string, pid: string): Observable<any> {

    if (!file) {
      return of(null);
    }

    return this.loadImage(file)
      .pipe(take(1))
      .pipe(switchMap(
        (img) => this.imgTbBlob(img, file)
      ))
      .pipe(take(1))
      .pipe(switchMap(blob => {
        const filePath = `prouduct/${uid}/${pid}`;
        this.ref = this.storage.ref(filePath);
        const task = this.ref.put(blob);
        // get notified when the download URL is available
        return task.snapshotChanges();
      }))
      .pipe(
        last(),
        switchMap(() => this.ref.getDownloadURL())
      );
  }

  deleteImage(url: string): Observable<string> {
    if (!url) {
      return of(null);
    }
    return from(
      this.storage.storage.refFromURL(url).delete()
    );

  }

  getImageUrlFromFile(file: File) {

    return this.loadImage(file).pipe(take(1))
      .pipe(map(img => img.toDataURL()));
  }

  copy(uid: string, opid: string, npid: string): Observable<{}> {
    return this.authService.currentIdToken().pipe(switchMap(token => {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        })
      };
      const data = { uid, opid, npid };
      return this.httpClient.post(`${environment.cloud_funtions_api_url}/firebaseStorage`, data, httpOptions);
    })).pipe(take(1));
  }

  private imgTbBlob(img, file: File) {
    return new Observable(observer => {
      img.toBlob(blob => {
        observer.next(blob);
        observer.complete();
      }, file.type, 0.95);
    });
  }

  private loadImage(file: File): Observable<any> {
    return new Observable(observer => {
      loadImage(
        file,
        (img) => {
          observer.next(img);
          observer.complete();
        },
        this.loadImgOptions
      );
    });
  }
}
