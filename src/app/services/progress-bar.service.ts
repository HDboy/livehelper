import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {


  private dataSource = new BehaviorSubject<boolean>(false);
  isShowProgressBar = this.dataSource.asObservable();

  constructor() { }

  showProgressBar(value: boolean) {
    this.dataSource.next(value);
  }

}
