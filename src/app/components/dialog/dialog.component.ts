import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { FbPageModel } from './../../models/fb-page-model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  selectedValue = {
    liveId: null,
    page: null
  };


  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: FbPageModel[],
  ) { }

  ngOnInit(): void { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  start() {
    this.dialogRef.close(this.selectedValue);
  }

}
