import { AuthService } from './../../services/auth.service';
import { NotifyService } from './../../services/notify.service';
import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { FileService } from 'src/app/services/file.service';

import { ProductModel } from './../../models/product-model';
import { ProductService } from './../../services/product.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.scss']
})
export class ManageProductsComponent implements OnInit {

  products$: Observable<ProductModel[]>;

  displayedColumns: string[];

  constructor(
    private productService: ProductService,
    private fileService: FileService,
    private notify: NotifyService,
    private auth: AuthService,
    public translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.products$ = this.auth.user$
      .pipe(switchMap(user => {
        if (!user) { return of(null); }
        return this.productService.getProductByUserId(user.uid);
      }

      ));
    this.setDisplayColumns();
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.setDisplayColumns();
    });
  }

  setDisplayColumns() {
    this.translate.get('manageProduct.displayedColumns').subscribe(result => {
      const { order, title, code, price, quantity, size, color, edit, deleteBtn } = result;
      this.displayedColumns = [order, title, code, price, quantity, size, color, edit, deleteBtn];
    });
  }



  onProductIdChange(product) {
    console.log(product);
    this.fileService.deleteImage(product.protoUrl)
      .pipe(switchMap(() => this.productService.deleteFromId(product.id)))
      .pipe(take(1))
      .subscribe(() => this.notify.success('Delete Successfully'));
  }

}
