import { TranslateService } from '@ngx-translate/core';
import { NotifyService } from './../../services/notify.service';
import { SettingModel } from './../../models/setting-model';
import { take, switchMap } from 'rxjs/operators';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingService } from 'src/app/services/setting.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit, OnDestroy {

  uid: string;

  setting: SettingModel = {
    bankAccount: null,
    thanksMessage: null
  };

  subscription: Subscription;

  constructor(
    private settingService: SettingService,
    private auth: AuthService,
    private notify: NotifyService,
    private translate: TranslateService

  ) { }


  ngOnInit(): void {
    this.subscription = this.auth.user$.pipe(switchMap(user => {
      this.uid = user.uid;
      return this.settingService.get(this.uid);
    })).subscribe(setting => {
      if (setting) {
        this.setting = setting;
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save() {
    this.settingService.create(this.uid, this.setting)
      .subscribe(() => {
        this.notify.success(this.translate.instant('notify.saveSuccess'));
      });
  }

}
