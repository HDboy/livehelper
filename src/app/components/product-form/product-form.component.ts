import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileSystemDirectoryEntry, FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ProductModel } from 'src/app/models/product-model';
import { FileService } from 'src/app/services/file.service';
import { NotifyService } from 'src/app/services/notify.service';
import { ProductService } from 'src/app/services/product.service';
import { ProgressBarService } from 'src/app/services/progress-bar.service';

import { AuthService } from './../../services/auth.service';


@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnDestroy {

  cols;
  rowHeight;
  colspan;
  isMobile: boolean;
  imageUrl;
  product: ProductModel = {
    title: null,
    price: null,
    quantity: null,
    detail: null,
    color: null,
    code: null,
    size: null,
    protoUrl: null,
    uid: null
  };
  uid: string;
  id: string;
  userSubscription: Subscription;
  productUploadSubcription: Subscription;
  productSubcription: Subscription;

  public files: NgxFileDropEntry[] = [];
  file: File;

  constructor(
    private notify: NotifyService,
    private progressBar: ProgressBarService,
    private fileService: FileService,
    private auth: AuthService,
    private productService: ProductService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {

    if (window.innerWidth <= 550) {
      this.cols = 1;
      this.rowHeight = 650;
      this.colspan = 1;
      this.isMobile = true;

    } else {
      this.cols = 6;
      this.rowHeight = 700;
      this.colspan = 2;
      this.isMobile = false;
    }

    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.productSubcription = this.productService.getById(this.id)
        .subscribe(product => {
          this.product = product;
          this.imageUrl = product.protoUrl;
        });
    }
    this.userSubscription = this.auth.user$.subscribe(user => this.uid = user.uid);
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.productUploadSubcription?.unsubscribe();
    this.productSubcription?.unsubscribe();
  }

  onResize(event) {
    if (event.target.innerWidth <= 550) {
      this.cols = 1;
      this.rowHeight = 650;
      this.colspan = 1;
      this.isMobile = true;

    } else {
      this.cols = 6;
      this.rowHeight = 700;
      this.colspan = 2;
      this.isMobile = false;
    }
  }

  save(product: ProductModel) {
    console.log(product);
    if (this.id) {
      this.progressBar.showProgressBar(true);
      this.productUploadSubcription = this.fileService.uploadProductImg(this.file, this.uid, this.id)
        .pipe(switchMap(downloadURL => {
          if (downloadURL) {
            product.protoUrl = downloadURL;
            console.log('downloadURL', downloadURL);
          }
          return this.productService.update(product, this.id);
        })).subscribe(() => {
          // console.log(ref);
          this.progressBar.showProgressBar(false);
          this.notify.success('Add Product Successfully');
          this.location.back();
        });
    } else {
      product.uid = this.uid;
      this.progressBar.showProgressBar(true);
      const pid = this.productService.createId();
      this.productUploadSubcription = this.fileService.uploadProductImg(this.file, this.uid, pid)
        .pipe(switchMap(downloadURL => {
          product.protoUrl = downloadURL;
          console.log('downloadURL', downloadURL);
          return this.productService.add(product, pid);
        })).subscribe(() => {
          // console.log(ref);
          this.progressBar.showProgressBar(false);
          this.notify.success('Add Product Successfully');
          this.location.back();
        });
    }



  }


  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        // const reader = new FileReader();
        const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
        fileEntry.file((file: File) => {
          if (!validImageTypes.includes(file.type)) {
            // invalid file type code goes here.
            console.error('invalid file type', file.type);
            this.notify.error('Invalid file type !!!');
            this.files = null;
            this.file = null;
            return;
          }
          this.file = file;
          this.fileService.getImageUrlFromFile(file)
            .subscribe(url => this.imageUrl = url);

          // Here you can access the real file
          console.log(droppedFile.relativePath, file);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }

}
