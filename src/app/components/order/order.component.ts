import { Component, OnInit, OnDestroy } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FbPageModel } from 'src/app/models/fb-page-model';
import { AuthService } from 'src/app/services/auth.service';
import { FbService } from 'src/app/services/fb.service';
import { NotifyService } from 'src/app/services/notify.service';

import { OrderModel } from './../../models/order-model';
import { OrderService } from './../../services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy {

  orders: OrderModel[];
  backupOrders: OrderModel[];
  user;
  displayedColumns: string[];
  pages: FbPageModel[];
  selected;
  subscription: Subscription;

  constructor(
    private orderService: OrderService,
    private auth: AuthService,
    public translate: TranslateService,
    public notify: NotifyService,
    public fbService: FbService
  ) { }

  ngOnInit(): void {
    const fbAccessToken = localStorage.getItem('fbAccessToken');
    this.subscription = this.auth.user$
      .pipe(switchMap(user => {
        this.user = user;
        const fbid = this.user.providerData.find(p => p.providerId === 'facebook.com').uid;
        if (!fbAccessToken) {
          this.notify.error('Please Logout and Login again !!!');
          return;
        }
        return this.fbService.getPage(fbid, fbAccessToken);
      }))
      .pipe(switchMap(data => {
        this.pages = data;
        console.log(this.pages);
        return this.orderService.get(this.user.uid);
      }))
      .subscribe(orders => this.orders = this.backupOrders = orders);

    this.setDisplayColumns();
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.setDisplayColumns();
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setDisplayColumns() {
    this.translate.get('order.display').subscribe(result => {
      const { order, productName, productCode, productPrice, productColour, productSize, buyerName, date, pageName } = result;
      this.displayedColumns = [order, productName, productCode, productPrice, productColour, productSize, buyerName, pageName, date];
    });
  }



  onProductIdChange(product) {
    console.log(product);
    // this.fileService.deleteImage(product.protoUrl)
    //   .pipe(switchMap(() => this.productService.deleteFromId(product.id)))
    //   .pipe(take(1))
    //   .subscribe(() => this.notify.success('Delete Successfully'));
  }

  onSelectPage() {
    console.log(this.selected);
    if (this.selected === 'all') {
      this.orders = this.backupOrders;
    } else {
      this.orders = this.backupOrders.filter(o => o.page.id === this.selected.id);
    }
  }

}
