import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ProgressBarService } from 'src/app/services/progress-bar.service';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  isShow$: Observable<boolean>;
  constructor(private progressBarService: ProgressBarService) { }

  ngOnInit(): void {
    this.isShow$ = this.progressBarService.isShowProgressBar;
  }

}
