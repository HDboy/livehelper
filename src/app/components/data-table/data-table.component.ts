import { async } from '@angular/core/testing';

import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProductModel } from 'src/app/models/product-model';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() displayedColumns: string[];

  // tslint:disable-next-line:variable-name
  private _items;
  @Input()
  set items(val) {
    if (!val) {
      return;
    }
    this._items = val;
    this.addDataSource(this.items);
  }
  get items() {
    return this._items;
  }

  @Output() productIdEvent = new EventEmitter<ProductModel>();

  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(
    public translate: TranslateService
  ) {

  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      if (this.items && this.items[0].hasOwnProperty('product')) {
        this.setDataSourceOrder(this.items);
      } else {
        this.setDataSourceProduct(this.items);
      }
    });

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addDataSource(items) {
    if (items && items[0].hasOwnProperty('product')) {
      this.setDataSourceOrder(items);
    } else {
      this.setDataSourceProduct(items);
    }



  }

  setDataSourceProduct(products) {
    if (!products) {
      return;
    }
    const productsDataSource = products?.map((item, index) => {
      const { order, title, code, price, quantity, size, color } = this.translate.instant('manageProduct.displayedColumns');
      item[order] = index + 1;
      item[title] = item.title;
      item[code] = item.code;
      item[price] = item.price;
      item[quantity] = item.quantity;
      item[size] = item.size;
      item[color] = item.color;
      return item;
    });
    this.dataSource.data = productsDataSource;
  }

  setDataSourceOrder(orders) {
    if (!orders) {
      return;
    }
    const productsDataSource = orders?.map((item, index) => {
      const { order, productName, productCode, productPrice, productColour,
        productSize, buyerName, date, pageName } = this.translate.instant('order.display');
      item[order] = index + 1;
      item[productName] = item.product?.title;
      item[productCode] = item.product?.code;
      item[productPrice] = item.product?.price;
      item[productColour] = item.product?.color;
      item[productSize] = item.product?.size;
      item[buyerName] = item.user?.name;
      item[pageName] = item.page?.name;
      item[date] = typeof (item.date) === 'string' ? item.date : item.date?.toDate().toLocaleString();
      return item;
    });
    this.dataSource.data = productsDataSource;
  }

  delete(product) {
    // console.log(product);

    this.productIdEvent.emit(product);
  }
}


