import { ProgressBarService } from './../../services/progress-bar.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { faList, faPlusCircle, faSignOutAlt, faTasks, faUserCog } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import * as firebase from 'firebase';

import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faTasks = faTasks;
  faPlusCircle = faPlusCircle;
  faSignOutAlt = faSignOutAlt;
  faFacebook = faFacebook;
  faUserCog = faUserCog;
  faList = faList;
  user: firebase.User;
  loginBtnDisable: boolean;

  language = {
    display: null,
    image: null
  };

  constructor(
    private translate: TranslateService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private progressBar: ProgressBarService
  ) { }

  ngOnInit(): void {
    this.translate.addLangs(['en', 'th']);

    const language = localStorage.getItem('language');
    if (!language) {
      this.language = {
        display: 'ไทย',
        image: '/assets/images/flag/th.png'
      };
      this.translate.setDefaultLang('th');
    } else {
      this.translate.setDefaultLang(language);
      this.setLanguage(language);
    }

    this.auth.user$.subscribe(appUser => {
      this.user = appUser;
      console.log(appUser);
    });

    this.progressBar.isShowProgressBar.subscribe(isShow => this.loginBtnDisable = isShow);
  }

  login() {
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem('returnUrl', returnUrl);
    this.auth.loginWithFb();
  }

  logout() {
    this.auth.logout()
      .subscribe(() => this.router.navigate(['/']));
  }

  switchLang(lang: string) {
    this.translate.use(lang);
    localStorage.setItem('language', lang);
    this.setLanguage(lang);
  }

  setLanguage(lang: string) {
    this.language.display = lang === 'en' ? 'English' : 'ไทย';
    if (lang === 'en') {
      this.language.image = '/assets/images/flag/en.png';
    } else {
      this.language.image = '/assets/images/flag/th.png';
    }
  }

}
