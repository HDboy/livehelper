import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { Router } from '@angular/router';
import { faCircle, faPlayCircle, faStopCircle } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import * as firebase from 'firebase';
import { from, of, Subscription } from 'rxjs';
import { concatMap, delay, switchMap, take } from 'rxjs/operators';
import { FbMessageModel } from 'src/app/models/fb-message-model';
import { FbService } from 'src/app/services/fb.service';
import { FileService } from 'src/app/services/file.service';
import { NotifyService } from 'src/app/services/notify.service';
import { ProductService } from 'src/app/services/product.service';
import { SettingService } from 'src/app/services/setting.service';

import { ProductModel } from './../../models/product-model';
import { SettingModel } from './../../models/setting-model';
import { AuthService } from './../../services/auth.service';
import { DialogComponent } from './../dialog/dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  products: ProductModel[];
  selectedPage = {
    liveId: null,
    page: null
  };

  faPlayCircle = faPlayCircle;
  faStopCircle = faStopCircle;
  faCircle = faCircle;

  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;

  contextMenuPosition = { x: '0px', y: '0px' };
  subcription: Subscription;
  user: firebase.User;
  setting: SettingModel;
  isConnected: boolean;
  isConnectedSub: Subscription;

  constructor(
    private fbService: FbService,
    private productService: ProductService,
    private fileService: FileService,
    private notify: NotifyService,
    private auth: AuthService,
    private dialog: MatDialog,
    private settingService: SettingService,
    private translate: TranslateService,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.subcription = this.auth.user$.pipe(switchMap(user => {
      this.user = user;
      console.log(user);
      if (!user) {
        return of(null);
      }
      return this.productService.getProductByUserId(user.uid);
    })).subscribe(products => this.products = products);

    this.isConnectedSub = this.fbService.isConnected$
      .subscribe(isConnected => this.isConnected = isConnected);
  }

  ngOnDestroy(): void {
    this.subcription?.unsubscribe();
    this.isConnectedSub.unsubscribe();
  }


  onContextMenu(event: MouseEvent, item) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menuData = { item };
    this.contextMenu.menu.focusFirstItem('mouse');
    this.contextMenu.openMenu();
  }

  onContextMenuDelete(product) {
    console.log(product);
    this.fileService.deleteImage(product.protoUrl)
      .pipe(switchMap(() => this.productService.deleteFromId(product.id)))
      .pipe(take(1))
      .subscribe(() => this.notify.success(this.translate.instant('common.deleteSuccessfully')));
  }

  onContextMenuDuplicateProduct(product: ProductModel & { id }) {
    const oldId = product.id;
    const newId = this.productService.createId();
    const uid = product.uid;
    console.log('old Id: ', oldId);
    console.log('new Id: ', newId);
    this.fileService.copy(uid, oldId, newId)
      .pipe(switchMap(url => {
        product.protoUrl = (url as any)?.returnUrl;
        product.id = newId;
        return this.productService.add(product, product.id);
      }))
      .pipe(take(1))
      .subscribe(() => this.router.navigate(['/product/edit', product.id]));
  }

  startApp() {
    const fbAccessToken = localStorage.getItem('fbAccessToken');
    const fbid = this.user.providerData.find(p => p.providerId === 'facebook.com').uid;
    if (!fbAccessToken) {
      this.notify.error('Please Logout and Login again !!!');
      return;
    }

    this.settingService.get(this.user.uid).pipe(take(1))
      .subscribe(setting => this.setting = setting);

    this.fbService.getPage(fbid, fbAccessToken).subscribe(data => {
      console.log(data);
      const dialogRef = this.dialog.open(DialogComponent, {
        data
      });

      dialogRef.afterClosed()
        .pipe(switchMap(result => {
          console.log(result);
          this.selectedPage = result;
          if (!this.selectedPage) { return of(null); }
          return this.fbService.getLiveComment(this.selectedPage.liveId, fbAccessToken);
        }))
        .subscribe(result => {
          if (!result) { return; }
          const liveComment = JSON.parse((result as any).data) as FbMessageModel;
          console.log(liveComment);
          this.productService.checkStock(
            this.products,
            liveComment.message,
            liveComment.from.id,
            this.selectedPage.page,
            liveComment.from.name,
            this.setting
          );
        });
    });


    // from(['c01 red 52', '54 C01 red', 'c0154red', ' 52redc01'])
    //   .pipe(
    //     concatMap(val => of(val).pipe(delay(1000))),
    //   )
    //   .subscribe(code => {
    //     console.log('code', code);
    //     this.productService.checkStock(this.products, code);
    //   });
  }

  stopApp() {
    this.fbService.stopLiveComment();
  }

  test() {
    from(['s002m', 's11 red', 's001red', ' 52redc01'])
      .pipe(
        concatMap(val => of(val).pipe(delay(1000))),
      )
      .subscribe(code => {
        console.log('code', code);
        this.productService.checkStock(this.products, code, '10157683095394227', this.selectedPage.page, 'hady', this.setting);
      });
  }

}
