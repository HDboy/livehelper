export interface SettingModel {
    bankAccount: string;
    thanksMessage: string;
}
