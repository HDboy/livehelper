export interface FbUserModel {
    name: string;
    id: string;
    psid?: string;
}
