
export interface UserModel {
    uid: string;
    email: string;
    role: string;
    phoneNumber: string;
    fbid: string;
}
