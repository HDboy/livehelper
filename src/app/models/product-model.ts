export interface ProductModel {
    title: string;
    price: number;
    code: string;
    color: string;
    quantity: number;
    detail: string;
    size: string;
    protoUrl: string;
    uid: string;
}
