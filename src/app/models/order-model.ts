import { ProductModel } from './product-model';
import { FbUserModel } from './fb-user-model';
import { FbPageModel } from './fb-page-model';

export interface OrderModel {
    user: FbUserModel;
    product: ProductModel;
    id: string;
    date: Date;
    uid: string;
    page: FbPageModel;
}
