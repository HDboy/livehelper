export interface FbPageModel {
    access_token?: string;
    id: string;
    name: string;
}
