import { FbUserModel } from './fb-user-model';
export interface FbMessageModel {
    from: FbUserModel;
    message: string;
    id: string;
}
